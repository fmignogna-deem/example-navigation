import Foundation

public struct LoginQuery: ApolloQuery {
    public typealias Response = LoginResult

    public let username: String
    public let password: String

    public init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}

public struct LoginResult: ApolloResponse {
    public let username: String

    public init(data: Data) {
        self.username = String(data: data, encoding: .utf8) ?? ""
    }
}
