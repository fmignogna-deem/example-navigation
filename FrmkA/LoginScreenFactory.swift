import SwiftUI
import UIKit

// I could have used Enum, Struct or only a function. Doesn't matter.
public final class LoginScreenFactory {
    private init() {}

    public static func make(onLoginSuccess: @escaping (String) -> Void, loader: UserLoaderRemote) -> UIViewController {
        let loginUseCase = LoginUseCase(loader: loader)
        let viewModel = LoginViewModel(loginUseCase: loginUseCase)
        return LoginViewController(onLoginSuccess: onLoginSuccess, viewModel: viewModel)
    }
}
