import Foundation

final class LoginViewModel {

    private let loginUseCase: LoginUseCase

    init(loginUseCase: LoginUseCase) {
        self.loginUseCase = loginUseCase
    }

    var onSuccess: ((String) -> Void)?
    var onFailure: (() -> Void)?

    func login(username: String, password: String) {
        loginUseCase.login(username: username, password: password) { [weak self] result in
            switch result {
            case let .success(username):
                self?.onSuccess?(username)
            case .failure:
                self?.onFailure?()
            }
        }
    }
}
