import UIKit
import SwiftUI

public final class OptionSelectionScreenFactory {
    private init() {}

    public static func makeUIKit(options: [Option], onSelection: @escaping (Option) -> Void) -> UIViewController {
        let view = OptionsSelectionView(
            options: options,
            selection: onSelection
        )
        let hostingController = UIHostingController(rootView: view)
        return hostingController
    }
}
