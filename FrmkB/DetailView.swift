//
//  DetailView.swift
//  FrmkB
//
//  Created by Fabio Mignogna on 10/08/2022.
//

import SwiftUI

public protocol DetailViewCallback {
    func didSelectOption(_ option: Option)
}

internal struct DetailView: View, DetailViewCallback {
    @State private var optionSelected: Option?

    let selectionOption: (DetailViewCallback) -> Void

    var body: some View {
        VStack(spacing: 20) {
            if let optionSelected = optionSelected {
                Text("Option selected: \(optionSelected.description)")
            }
            Button(action: { selectionOption(self) }) {
                Text("Select an option")
            }
        }
    }

    func didSelectOption(_ option: Option) {
        self.optionSelected = option
    }
}
