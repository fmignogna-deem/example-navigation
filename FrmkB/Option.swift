//
//  Option.swift
//  FrmkB
//
//  Created by Fabio Mignogna on 16/08/2022.
//

import Foundation

public struct Option {
    public let id: UUID
    public let description: String

    public init(id: UUID = UUID(), description: String) {
        self.id = id
        self.description = description
    }
}
