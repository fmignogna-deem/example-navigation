import UIKit

internal final class MainViewController: UIViewController {

    private var user: String?
    private var tapped: (() -> Void)?

    private lazy var userLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .body)
        label.text = "You are logged in as \(user ?? "<nothing>")"
        label.textAlignment = .center
        return label
    }()

    private lazy var loginButton: UIButton = {
        let btn = UIButton(type: .roundedRect)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Detail", for: .normal)
        btn.titleLabel?.font = .preferredFont(forTextStyle: .largeTitle)
        btn.addTarget(self, action: #selector(loginButtonOnTap), for: .touchUpInside)
        return btn
    }()

    convenience init(user: String, tapped: @escaping () -> Void) {
        self.init()
        self.tapped = tapped
        self.user = user
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        title = "Main"
        view.backgroundColor = .white
        view.addSubview(userLabel)
        view.addSubview(loginButton)

        NSLayoutConstraint.activate([
            userLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            userLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            userLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    @objc private func loginButtonOnTap() {
        tapped?()
    }
}
